# Epicode Books
Gestione di catalago di una biblioteca

##Specifiche

La libreria gestisce nel catalogo libri e periodici.

Per ogni libro è necessario gestire:
- Titolo
- Autori(possono essere più di uno)
- Editore
- Data di pubblicazione
- Codice ISBN (se presente)
- Codice  interno della biblioteca
- Numero di copie a disposizione
- Volumi in cui è strutturato
- Lingua
- Categoria di classificazione (anche piu di una)
- Numero di pagine totali ( di tutti i volumi)

Ogni copia a disposizione ha:
- Una posizione nella biblioteca
- Una indicazione che comunica se disponibile per il prestito o meno
- Uno stato ( in consultazione, in prestito, disponibile)

Ogni volume ha:
- Titolo
- Codice ISBN (se presente)
- Codice  interno della biblioteca
- Numero di pagine

Ogni Autore è caratterizzato da:
- Nome
- Cognome
- Nazionalità 
- Anno di nascita
- Anno di morte(se non in vita)

I periodici sono caratterizzati da:
- Data di pubblicazione
- Editore
- Titolo
- Categoria di classificazione (anche più di una)
- Periodicità  (quotidiano, mensile, annuale,estemporanea, ecc)

##Permessi

Gli operatori della biblioteca possonmo gestire il catalogo, mentre gli utenti possono solo fare delle ricerche.
