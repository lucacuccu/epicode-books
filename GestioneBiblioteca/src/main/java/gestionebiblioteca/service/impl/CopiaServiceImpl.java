package gestionebiblioteca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Copia;
import gestionebiblioteca.enities.data.Volume;
import gestionebiblioteca.enities.model.Stato;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.CopiaRepository;
import gestionebiblioteca.service.CopiaService;

@Service
public class CopiaServiceImpl implements CopiaService {

	@Autowired
	CopiaRepository copiaRepo;

	@Override
	public Copia save(Copia copia) {
		try {
			return copiaRepo.save(copia);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Copia edit(Copia copia) {
		try {
			var c = copiaRepo.findById(copia.getId()).get();
			
			if (copia.getPosizione() != null)
				if (copia.getPosizione().isBlank())
					c.setPosizione(copia.getPosizione());
			if (copia.isDisponibile()) c.setDisponibile(copia.isDisponibile());
			if (copia.getStato()!=null) c.setStato(copia.getStato());
			if (copia.getVolume()!= null) c.setVolume(copia.getVolume());
			
			return copiaRepo.save(c);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
	@Override
	public Copia delete(Copia copia) {
		try {
			var c = copiaRepo.findById(copia.getId()).get();
			copiaRepo.delete(c);
			return c;
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Copia> getByStato(String stato) {
		try {
			Stato s = Stato.valueOf(stato.toUpperCase());
			return copiaRepo.findByStato(s);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Copia> getByPosizione(String posizione) {
		try {
			return copiaRepo.findByPosizione(posizione);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Copia> getByVolume(Volume volume) {
		try {
			return copiaRepo.findByVolume(volume);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

}
