package gestionebiblioteca.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Categoria;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.CategoriaRepository;
import gestionebiblioteca.service.CategoriaService;

@Service
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	CategoriaRepository categoriaRepo;
	
	@Override
	public Categoria getByNome(String nome) {
		try {
			return categoriaRepo.findByCategoria(nome);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Categoria save(Categoria categoria) {
		try {
			return categoriaRepo.save(categoria);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
	@Override
	public Categoria update(Categoria categoria) {
		try {
			var a = categoriaRepo.findById(categoria.getId()).get();
			
			if(categoria.getCategoria()!=null) if(!categoria.getCategoria().isBlank()) a.setCategoria(categoria.getCategoria());
			
			return categoriaRepo.save(a);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

}