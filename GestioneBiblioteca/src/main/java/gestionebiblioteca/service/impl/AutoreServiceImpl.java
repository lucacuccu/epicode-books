package gestionebiblioteca.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Autore;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.AutoreRepository;
import gestionebiblioteca.service.AutoreService;

@Service
public class AutoreServiceImpl implements AutoreService {
	
	@Autowired
	AutoreRepository autoreRepo;
	
	@Override
	public Autore save(Autore autore) {
		try {
			return autoreRepo.save(autore);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
	@Override
	public Autore update(Autore autore) {
		try { 
			var a = autoreRepo.findById(autore.getId()).get();
			
			if(autore.getNome()!=null) if(!autore.getNome().isBlank()) a.setNome(autore.getNome());
			if(autore.getCognome()!=null) if(!autore.getCognome().isBlank()) a.setCognome(autore.getCognome());
			if(autore.getNazionalita()!=null) if(!autore.getNazionalita().isBlank()) a.setNazionalita(autore.getNazionalita());
			if(autore.getAnnoNascita()!=null) a.setAnnoNascita(autore.getAnnoNascita());
			if(autore.getAnnoMorte()!=null) a.setAnnoMorte(autore.getAnnoMorte());
			
			return autoreRepo.save(a);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Autore> getByNome(String nome) {
		try {
			return autoreRepo.findByNome(nome);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Autore> getByCognome(String cognome) {
		try {
			return autoreRepo.findByCognome(cognome);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Autore> getByAnnoNascita(LocalDate locaDate) {
		try {
			return autoreRepo.findByAnnoNascita(locaDate);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Autore> getByNomeAndCognome(Autore autore) {
		try {
			return autoreRepo.findByNomeAndCognome(autore.getNome(), autore.getCognome());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

}
