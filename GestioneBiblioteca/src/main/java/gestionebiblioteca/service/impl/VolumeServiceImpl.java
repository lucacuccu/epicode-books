package gestionebiblioteca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Libro;
import gestionebiblioteca.enities.data.Volume;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.LibroRepository;
import gestionebiblioteca.repository.VolumeRepository;
import gestionebiblioteca.service.VolumeService;

@Service
public class VolumeServiceImpl implements VolumeService {
	
	@Autowired
	VolumeRepository volumeRepo;
	
	@Autowired
	LibroRepository libroRepo;
	
	@Override
	public Volume save(Volume volume) {
		try {
			return volumeRepo.save(volume);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Volume delete(Volume volume) {
		try {
			var v = volumeRepo.findByCodiceInterno(volume.getCodiceInterno());
			volumeRepo.delete(v);
			return v;
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Volume update(Volume volume) {
		try {
			var v = volumeRepo.findById(volume.getId()).get();
			
			if(volume.getTitolo()!=null) 
				if(volume.getTitolo().isBlank()) 
					v.setTitolo(volume.getTitolo());
			if(volume.getCodiceISBN()!=null) v.setCodiceISBN(volume.getCodiceISBN());
			if(volume.getCodiceInterno()!=null) v.setCodiceInterno(volume.getCodiceInterno());
			if(volume.getNumeroPagine()!=null) v.setNumeroPagine(volume.getNumeroPagine());
			
			var l = libroRepo.findByCodiceInterno(volume.getLibro().getCodiceInterno());
			
			if(volume.getLibro()!=null) v.setLibro(l);
			
			return volumeRepo.save(v);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Volume> getByTitolo(Volume volume) {
		try {
			return volumeRepo.findByTitolo(volume.getTitolo());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Volume getByCodiceISBN(Volume volume) {
		try {
			return volumeRepo.findByCodiceISBN(volume.getCodiceISBN());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Volume getByCodiceInterno(Volume volume) {
		try {
			return volumeRepo.findByCodiceInterno(volume.getCodiceInterno());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Volume> getByLibro(Libro libro) {
		try {
			var l = libroRepo.findByCodiceInterno(libro.getCodiceInterno());
			return volumeRepo.findByLibro(l);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
}
