package gestionebiblioteca.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Lingua;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.LibroRepository;
import gestionebiblioteca.repository.LinguaRepository;
import gestionebiblioteca.service.LinguaService;

@Service
public class LinguaServiceImpl implements LinguaService{
	
	@Autowired
	LinguaRepository linguaRepo;
	
	@Autowired
	LibroRepository libroRepo;

	@Override
	public Lingua getByNome(String lingua) {
		try {
			return linguaRepo.findByLingua(lingua);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Lingua save(Lingua lingua) {
		try {
			return linguaRepo.save(lingua);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Lingua update(Lingua lingua) {
		try {
			var l = linguaRepo.findByLingua(lingua.getLingua());
			return linguaRepo.save(l);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
//	@Override
//	public Lingua delete(Lingua lingua) {
//		try {
//			var allLibri = libroRepo.findAll();
//			for (Libro l : allLibri) {
//				l.setLingua(null);
//			}
//			var l = linguaRepo.findByLingua(lingua.getLingua());
//			return linguaRepo.save(l);
//		} catch (Exception e) {
//			throw new ApplicationExcption(e);
//		}
//	}
	
}
