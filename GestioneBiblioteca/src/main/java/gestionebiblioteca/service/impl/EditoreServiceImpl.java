package gestionebiblioteca.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Editore;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.EditoreRepository;
import gestionebiblioteca.service.EditoreService;

@Service
public class EditoreServiceImpl implements EditoreService {

	@Autowired
	EditoreRepository editoreRepo;

	@Override
	public Editore getByNome(String nome) {
		try {
			return editoreRepo.findByNomeEditore(nome);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Editore save(Editore editore) {
		try {
			return editoreRepo.save(editore);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Editore update(Editore editore) {
		try {

			var e = editoreRepo.findById(editore.getId()).get();

			if (editore.getNomeEditore() != null)
				if (editore.getNomeEditore().isBlank())
					editore.setNomeEditore(editore.getNomeEditore());;

			return editoreRepo.save(e);
			
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

}
