package gestionebiblioteca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Categoria;
import gestionebiblioteca.enities.data.Editore;
import gestionebiblioteca.enities.data.Periodico;
import gestionebiblioteca.enities.model.Periodicita;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.CategoriaRepository;
import gestionebiblioteca.repository.EditoreRepository;
import gestionebiblioteca.repository.PeriodicoRepository;
import gestionebiblioteca.service.PeriodicoService;

@Service
public class PeriodicoServiceImpl implements PeriodicoService {
	
	@Autowired
	PeriodicoRepository periodicoRepo;
	
	@Autowired
	EditoreRepository editoreREpo;
	
	@Autowired
	CategoriaRepository categoriaRepo;

	@Override
	public Periodico save(Periodico periodico) {
		try {
			return periodicoRepo.save(periodico);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Periodico update(Periodico periodico) {
		try {
			var p = periodicoRepo.findById(periodico.getId()).get();
			
			if(periodico.getTitolo()!=null) if(periodico.getTitolo().isBlank()) p.setTitolo(periodico.getTitolo());
			if(periodico.getDataPubblicazione()!=null) p.setDataPubblicazione(periodico.getDataPubblicazione());
			if(periodico.getEditore()!=null) p.setEditore(periodico.getEditore());
			if(periodico.getPeriodicita()!=null) p.setPeriodicita(periodico.getPeriodicita());			
			
			return periodicoRepo.save(p);
			
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
	@Override
	public Periodico saveCategoria(Periodico periodico, Categoria categoria) {
		try {
			var c = categoriaRepo.findByCategoria(categoria.getCategoria());
			
			var p = periodicoRepo.findByTitolo(periodico.getTitolo());
			p.getCategoria().add(c);
			
			return periodicoRepo.save(p);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Periodico delete(Periodico periodico) {
		try {
			
			var p = periodicoRepo.findByTitolo(periodico.getTitolo());
			
			var list = p.getCategoria();
			list.clear();
			
			periodicoRepo.delete(periodico);
			return p;
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
	@Override
	public Periodico getByTitolo(Periodico periodico) {
		try {
			return periodicoRepo.findByTitolo(periodico.getTitolo());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Periodico> getByDataPubblicazione(Periodico periodico) {
		try {
			return periodicoRepo.findByDataPubblicazione(periodico.getDataPubblicazione());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Periodico> getByEditore(Editore editore) {
		try {
			var e = editoreREpo.findByNomeEditore(editore.getNomeEditore());
			return periodicoRepo.findByEditore(e);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Periodico getByTitoloAndEditore(Periodico periodico, Editore editore) {
		try {
			var e = editoreREpo.findByNomeEditore(editore.getNomeEditore());
			return periodicoRepo.findByTitoloAndEditore(periodico.getTitolo(), e);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Periodico> getByPeriodicita(String periodicita) {
		try {
			Periodicita p = Periodicita.valueOf(periodicita);
			return periodicoRepo.findByPeriodicita(p);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}	

}

















