package gestionebiblioteca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Autore;
import gestionebiblioteca.enities.data.Categoria;
import gestionebiblioteca.enities.data.Libro;
import gestionebiblioteca.exception.ApplicationExcption;
import gestionebiblioteca.repository.AutoreRepository;
import gestionebiblioteca.repository.CategoriaRepository;
import gestionebiblioteca.repository.LibroRepository;
import gestionebiblioteca.service.LibroService;

@Service
public class LibroServiceImpl implements LibroService {
	
	@Autowired
	LibroRepository libroRepo;
	
	@Autowired
	AutoreRepository autoreRepo;
	
	@Autowired
	CategoriaRepository categoriaRepo;

	@Override
	public Libro save(Libro libro) {
		try {
			return libroRepo.save(libro);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Libro update(Libro libro) {
		try {
			
			var l = libroRepo.findById(libro.getId()).get();
			
			if(libro.getTitolo()!=null) if(libro.getTitolo().isBlank()) l.setTitolo(libro.getTitolo());
			if(libro.getEditore()!=null) l.setEditore(libro.getEditore());
			if(libro.getDataPubblicazione()!=null) l.setDataPubblicazione(libro.getDataPubblicazione());
			if(libro.getCodiceISBN()!=null) l.setCodiceISBN(libro.getCodiceISBN());
			if(libro.getCodiceInterno()!=null) l.setCodiceInterno(libro.getCodiceInterno());
			if(libro.getCopieDiponibili()!=null) l.setCopieDiponibili(libro.getCopieDiponibili());
			if(libro.getLingua()!=null) l.setLingua(libro.getLingua());
			if(libro.getNumeroPagine()!=null) l.setNumeroPagine(libro.getNumeroPagine());
			
			return libroRepo.save(l);
			
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}
	
	@Override
	public Libro saveCategoria(Libro libro, Categoria categoria) {
		
		try {
			var l = libroRepo.findById(libro.getId()).get();
			var c = categoriaRepo.findByCategoria(categoria.getCategoria());
			
			var a = l.getCategoria();
			
			a.add(c);
			
			return libroRepo.save(l);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
		
	}
	
	@Override
	public Libro saveAutore(Libro libro, Autore autore) {
		
		try {
			var l = libroRepo.findById(libro.getId()).get();
			var c = autoreRepo.findById(autore.getId()).get();
			
			var a = l.getAutore();
			
			a.add(c);
			
			return libroRepo.save(l);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
		
	}

	@Override
	public Libro delete(Libro libro) {
		try {
			
			var l = libroRepo.findByCodiceISBN(libro.getCodiceISBN());
			
//			var listaCategorie = l.getCategoria();
//			listaCategorie.clear();
//			
//			var listaAutori = l.getAutore();
//			listaAutori.clear();
		
			libroRepo.delete(l);
			return l;
			
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public List<Libro> getByTitolo(Libro libro) {
		try {
			return libroRepo.findByTitolo(libro.getTitolo());
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Libro getByCodiceISBN(Long codiceISBN) {
		try {
			return libroRepo.findByCodiceISBN(codiceISBN);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Libro getByCodiceInterno(Integer codiceInterno) {
		try {
			return libroRepo.findByCodiceInterno(codiceInterno);
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

	@Override
	public Libro getByCodiceIsbnAndAutore(Libro libro, Autore autore) {
		try {
			Libro find = new Libro();
			var l = libroRepo.findByCodiceISBN(libro.getCodiceISBN());
			var aut = autoreRepo.findById(autore.getId()).get();
			
			for(Autore a : l.getAutore()) {
				if(a.getId()==aut.getId()) {
					find = l;
				}
			}
			
			return find;
		} catch (Exception e) {
			throw new ApplicationExcption(e);
		}
	}

}
