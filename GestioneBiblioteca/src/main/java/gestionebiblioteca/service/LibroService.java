package gestionebiblioteca.service;

import java.util.List;

import gestionebiblioteca.enities.data.Autore;
import gestionebiblioteca.enities.data.Categoria;
import gestionebiblioteca.enities.data.Libro;

public interface LibroService {
	
	List<Libro> getByTitolo(Libro libro);
	Libro getByCodiceISBN(Long codiceISBN);
	Libro getByCodiceInterno(Integer codiceInterno);
	
	Libro getByCodiceIsbnAndAutore(Libro libro, Autore autore);
	
	Libro save(Libro libro);
	Libro update(Libro libro);
	Libro delete(Libro libro);
	//Libro updateCategoria(Libro libro, Categoria categoria, String nomeCategoria);
	Libro saveCategoria(Libro libro, Categoria categoria);
	Libro saveAutore(Libro libro, Autore autore);
	
	
}
