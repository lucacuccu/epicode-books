package gestionebiblioteca.service;

import gestionebiblioteca.enities.data.Lingua;

public interface LinguaService {
	
	Lingua getByNome(String lingua);
	Lingua save(Lingua lingua);
	Lingua update(Lingua lingua);
//	Lingua delete(Lingua lingua);
	
}
