package gestionebiblioteca.service;

import gestionebiblioteca.enities.data.Editore;

public interface EditoreService {
	
	Editore getByNome(String nome);

	Editore save(Editore editore);

	Editore update(Editore editore);

}
