package gestionebiblioteca.service;

import java.util.List;

import gestionebiblioteca.enities.data.Categoria;
import gestionebiblioteca.enities.data.Editore;
import gestionebiblioteca.enities.data.Periodico;

public interface PeriodicoService {
	
	Periodico getByTitolo(Periodico periodico);
	List<Periodico> getByDataPubblicazione(Periodico periodico);
	List<Periodico> getByEditore(Editore editore);
	Periodico getByTitoloAndEditore(Periodico periodico, Editore editore);
	List<Periodico> getByPeriodicita(String periodicita);
	
	Periodico save(Periodico periodico);
	Periodico update(Periodico periodico);
	Periodico delete(Periodico periodico);
	Periodico saveCategoria(Periodico periodico, Categoria categoria);

}
