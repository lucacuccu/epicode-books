package gestionebiblioteca.service;

import java.util.List;

import org.springframework.stereotype.Service;

import gestionebiblioteca.enities.data.Libro;
import gestionebiblioteca.enities.data.Volume;

@Service
public interface VolumeService {
	
	List<Volume> getByTitolo(Volume volume);
	Volume getByCodiceISBN(Volume volume);
	Volume getByCodiceInterno(Volume volume);
	List<Volume> getByLibro(Libro libro);
	
	Volume save(Volume volume);
	Volume delete(Volume volume);
	Volume update(Volume volume);
	
}
