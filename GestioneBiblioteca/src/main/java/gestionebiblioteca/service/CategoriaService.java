package gestionebiblioteca.service;

import gestionebiblioteca.enities.data.Categoria;

public interface CategoriaService {

	Categoria getByNome(String nome);

	Categoria save(Categoria categoria);

	Categoria update(Categoria categoria);
	
}
