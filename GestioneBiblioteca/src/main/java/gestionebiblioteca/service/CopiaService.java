package gestionebiblioteca.service;

import java.util.List;

import gestionebiblioteca.enities.data.Copia;
import gestionebiblioteca.enities.data.Volume;

public interface CopiaService {
	
	List<Copia> getByStato(String stato);
	List<Copia> getByPosizione(String posizione);
	List<Copia> getByVolume(Volume volume);
	Copia save(Copia copia);
	Copia edit(Copia copia);
	Copia delete(Copia copia);

}
