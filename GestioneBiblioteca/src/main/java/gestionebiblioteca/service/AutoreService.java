package gestionebiblioteca.service;

import java.time.LocalDate;
import java.util.List;

import gestionebiblioteca.enities.data.Autore;

public interface AutoreService {
	
	Autore save(Autore autore);
	Autore update(Autore autore);
	List<Autore> getByNome(String nome);
	List<Autore> getByNomeAndCognome(Autore autore);
	List<Autore> getByAnnoNascita(LocalDate locaDate);
	List<Autore> getByCognome(String cognome);

}
