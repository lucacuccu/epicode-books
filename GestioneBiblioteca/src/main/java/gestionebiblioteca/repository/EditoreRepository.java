package gestionebiblioteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Editore;

public interface EditoreRepository extends JpaRepository<Editore, Integer> {
	
	Editore findByNomeEditore(String nomeEditore);

}
