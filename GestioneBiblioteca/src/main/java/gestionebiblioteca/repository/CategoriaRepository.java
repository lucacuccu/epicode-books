package gestionebiblioteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {
	Categoria findByCategoria(String categoria);
}
