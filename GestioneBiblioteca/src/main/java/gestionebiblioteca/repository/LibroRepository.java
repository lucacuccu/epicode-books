package gestionebiblioteca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Libro;

public interface LibroRepository extends JpaRepository<Libro, Integer> {
	
	List<Libro> findByTitolo(String titolo);
	Libro findByCodiceISBN(long codiceISBN);
	Libro findByCodiceInterno(int codiceInterno);

}
