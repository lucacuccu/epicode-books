package gestionebiblioteca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Libro;
import gestionebiblioteca.enities.data.Volume;

public interface VolumeRepository extends JpaRepository<Volume, Integer> {

	List<Volume> findByTitolo(String titolo);
	Volume findByCodiceISBN(Long codiceISBN);
	Volume findByCodiceInterno(Integer codiceInterno);
	List<Volume> findByLibro(Libro libro);
	
}
