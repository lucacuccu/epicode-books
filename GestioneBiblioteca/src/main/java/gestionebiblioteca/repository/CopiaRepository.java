package gestionebiblioteca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Copia;
import gestionebiblioteca.enities.data.Volume;
import gestionebiblioteca.enities.model.Stato;

public interface CopiaRepository extends JpaRepository<Copia, Integer> {
	
	List<Copia> findByStato(Stato stato);
	List<Copia> findByPosizione(String posizione);
	List<Copia> findByVolume(Volume volume);

}
