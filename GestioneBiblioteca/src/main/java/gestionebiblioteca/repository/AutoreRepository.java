package gestionebiblioteca.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Autore;

public interface AutoreRepository extends JpaRepository<Autore, Integer> {
	
	List<Autore> findByNome(String nome);
	List<Autore> findByCognome(String cognome);
	List<Autore> findByNomeAndCognome(String nome, String cognome);
	List<Autore> findByAnnoNascita(LocalDate annoNascita);
	
}
