package gestionebiblioteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Lingua;

public interface LinguaRepository extends JpaRepository<Lingua, Integer> {
	
	Lingua findByLingua(String lingua);

}
