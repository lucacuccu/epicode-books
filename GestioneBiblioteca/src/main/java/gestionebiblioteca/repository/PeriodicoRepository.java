package gestionebiblioteca.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import gestionebiblioteca.enities.data.Editore;
import gestionebiblioteca.enities.data.Periodico;
import gestionebiblioteca.enities.model.Periodicita;

public interface PeriodicoRepository extends JpaRepository<Periodico, Integer> {
	
	Periodico findByTitolo(String titolo);
	List<Periodico> findByDataPubblicazione(LocalDate dataPubblicazione);
	List<Periodico> findByEditore(Editore editore);
	List<Periodico> findByPeriodicita(Periodicita periodicita);
	Periodico findByTitoloAndEditore(String titolo, Editore editore);

}
