package gestionebiblioteca.exception;

public class ApplicationExcption extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	
	public ApplicationExcption() {
		super();
	}
	
	public ApplicationExcption(Throwable e) {
		super(e);
	}

}
