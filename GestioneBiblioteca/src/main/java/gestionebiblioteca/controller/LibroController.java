package gestionebiblioteca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Libro;
import gestionebiblioteca.service.impl.LibroServiceImpl;

@RestController
@RequestMapping("/libro")
public class LibroController {

	@Autowired
	LibroServiceImpl libroImpl;
	
	@GetMapping("/getbytitolo")
	public List<Libro> getByTitolo(@RequestBody Libro libro) {
		return libroImpl.getByTitolo(libro);
	}
	@GetMapping("/getbycodiceisbn")
	public Libro getByCodiceISBN(@RequestParam Long codiceIsbn) {
		return libroImpl.getByCodiceISBN(codiceIsbn);
	}
	
	@GetMapping("/deletelibro")
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Libro delete(@RequestBody Libro libro) {
		return libroImpl.delete(libro);
	}
	@PostMapping("/salvalibro")
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Libro salva(@RequestBody Libro libro) {
		return libroImpl.save(libro);
	}
	
	@PostMapping("/updatelibro")
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Libro update(@RequestBody Libro libro) {
		return libroImpl.update(libro);
	}
}
