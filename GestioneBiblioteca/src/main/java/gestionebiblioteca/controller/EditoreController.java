package gestionebiblioteca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Editore;
import gestionebiblioteca.service.impl.EditoreServiceImpl;

@RestController
@RequestMapping("/editore")
public class EditoreController {

	@Autowired
	EditoreServiceImpl editoreImpl;
	
	@GetMapping("/getbynome")
	public Editore getbynome(@RequestParam String nome){
		return editoreImpl.getByNome(nome);
    }
	@PostMapping("/salva")
	public Editore salva(@RequestBody Editore editore){
		return editoreImpl.save(editore);
    }
	@PostMapping("/update")
	public Editore update(@RequestBody Editore editore){
		return editoreImpl.update(editore);
    }
}
