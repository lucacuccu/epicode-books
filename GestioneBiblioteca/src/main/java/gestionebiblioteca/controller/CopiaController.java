package gestionebiblioteca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Copia;
import gestionebiblioteca.enities.data.Volume;
import gestionebiblioteca.service.impl.CopiaServiceImpl;

@RestController
@RequestMapping("/copia")
public class CopiaController {

	@Autowired
	CopiaServiceImpl copiaImpl;
	
	@GetMapping("/getbyposizione")
	public List<Copia> getByPosizione(@RequestParam String posizione){
		return copiaImpl.getByPosizione(posizione);
	}
	@GetMapping("/getbystato")
	public List<Copia> getByStato(@RequestParam String stato){
		return copiaImpl.getByStato(stato);
	}
	@GetMapping("/getbyvolume")
	public List<Copia> getByVolume(@RequestParam Volume volume){
		return copiaImpl.getByVolume(volume);
	}
	@PostMapping("/update")
	public Copia update(@RequestParam Copia copia){
		return copiaImpl.edit(copia);
	}
	@PostMapping("/salva")
	public Copia salva(@RequestParam Copia copia){
		return copiaImpl.save(copia);
	}
}









