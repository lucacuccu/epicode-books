package gestionebiblioteca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Categoria;
import gestionebiblioteca.service.impl.CategoriaServiceImpl;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
	
	@Autowired
	CategoriaServiceImpl categoriaImpl;
	
	@GetMapping("/getbynome")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Categoria getByNome(@RequestParam String nome) {
		return categoriaImpl.getByNome(nome);
	}
	
	@PostMapping("/save")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Categoria save(@RequestBody Categoria categoria) {
		return categoriaImpl.save(categoria);
	}
	
	@PostMapping("/update")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Categoria update(@RequestBody Categoria categoria) {
		return categoriaImpl.update(categoria);
	}

}
