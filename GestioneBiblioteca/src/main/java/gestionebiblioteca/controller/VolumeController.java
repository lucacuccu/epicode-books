package gestionebiblioteca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Libro;
import gestionebiblioteca.enities.data.Volume;
import gestionebiblioteca.service.impl.VolumeServiceImpl;

@RestController
@RequestMapping("/volume")
public class VolumeController {

	@Autowired
	VolumeServiceImpl volumeImpl;
	
	@GetMapping("/getbytitolo")
	public List<Volume> getByTitolo(@RequestBody Volume volume){
		return volumeImpl.getByTitolo(volume);
	}
	@GetMapping("/getbycodiceisbn")
	public Volume getByCodiceISBN(@RequestBody Volume volume){
		return volumeImpl.getByCodiceISBN(volume);
	}
	@GetMapping("/getbycodiceinterno")
	public Volume getByCodiceInterno(@RequestBody Volume volume){
		return volumeImpl.getByCodiceInterno(volume);
	}
	@GetMapping("/getbylibro")
	public List<Volume> getByLibro(@RequestBody Libro libro){
		return volumeImpl.getByLibro(libro);
	}
	@PostMapping("/salva")
	public Volume salva(@RequestBody Volume volume){
		return volumeImpl.save(volume);
	}
	@PostMapping("/update")
	public Volume update(@RequestBody Volume volume){
		return volumeImpl.update(volume);
	}
	@PostMapping("/delete")
	public Volume delete(@RequestBody Volume volume){
		return volumeImpl.delete(volume);
	}
}
