package gestionebiblioteca.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Autore;
import gestionebiblioteca.service.impl.AutoreServiceImpl;

@RestController
@RequestMapping("/autore")
public class AutoreController {

	@Autowired
	AutoreServiceImpl autoreImpl;
	
	@GetMapping("/getbynome")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Autore> getByNome(@RequestParam String nome) {
		return autoreImpl.getByNome(nome);
	}
	
	@GetMapping("/getbycognome")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Autore> getByCognome(@RequestParam String cognome) {
		return autoreImpl.getByCognome(cognome);
	}
	
	@GetMapping("/getbyannonasita")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Autore> getByAnnoNascita(@RequestParam int giorno, @RequestParam int mese, @RequestParam int anno) {
		
		LocalDate l = LocalDate.of(anno, mese, giorno);
		
		return autoreImpl.getByAnnoNascita(l);
	}
	
	@PostMapping("/save")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Autore save(@RequestBody Autore autore) {
		
		return autoreImpl.save(autore);
	}
	
	@PostMapping("/update")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Autore update(@RequestBody Autore autore) {
		return autoreImpl.update(autore);
	}
	
}
