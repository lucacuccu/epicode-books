package gestionebiblioteca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gestionebiblioteca.enities.data.Lingua;
import gestionebiblioteca.service.impl.LinguaServiceImpl;

@RestController
@RequestMapping("/lingua")
public class LinguaController {

	@Autowired
	LinguaServiceImpl linguaImpl;
	
	@GetMapping("/getbynomelingua")
	public Lingua getByNomeLingua(@RequestParam String lingua){
		return linguaImpl.getByNome(lingua);
	}
	@PostMapping("/salva")
	public Lingua salva(@RequestBody Lingua lingua){
		return linguaImpl.save(lingua);
	}
	@PostMapping("/update")
	public Lingua update(@RequestBody Lingua lingua){
		return linguaImpl.update(lingua);
	}
}
