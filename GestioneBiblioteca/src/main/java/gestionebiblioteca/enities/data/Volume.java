package gestionebiblioteca.enities.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gestionebiblioteca.enities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Volume extends BaseEntity {
	
	private String titolo;
	@Column(unique = true)
	private Long codiceISBN;
	@Column(unique = true)
	private Integer codiceInterno;
	private Integer numeroPagine;
	
	@JsonIgnore
    @ToString.Exclude
	@ManyToOne
	private Libro libro;
	
}
