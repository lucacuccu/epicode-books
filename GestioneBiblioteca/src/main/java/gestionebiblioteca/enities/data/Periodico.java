package gestionebiblioteca.enities.data;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gestionebiblioteca.enities.BaseEntity;
import gestionebiblioteca.enities.model.Periodicita;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Periodico extends BaseEntity {
	
	@Column(unique = true)
	private String titolo;
	private LocalDate dataPubblicazione;
	
	@JsonIgnore
    @ToString.Exclude
	@ManyToOne
	private Editore editore;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(
	        name="periodico_categoria",
	        joinColumns= @JoinColumn(name="periodico_id", referencedColumnName="id"),
	        inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
	    )
	private Set<Categoria> categoria;
	
	@Enumerated(EnumType.STRING)
	private Periodicita periodicita;
	
}
