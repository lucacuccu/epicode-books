package gestionebiblioteca.enities.data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gestionebiblioteca.enities.BaseEntity;
import gestionebiblioteca.enities.model.Stato;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Copia extends BaseEntity {
	
	private String posizione;
	private boolean disponibile;
	@Enumerated(EnumType.STRING)
	private Stato stato;
	
	@JsonIgnore
    @ToString.Exclude
	@ManyToOne
	private Volume volume;

}
