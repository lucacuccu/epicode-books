package gestionebiblioteca.enities.data;

import javax.persistence.Column;
import javax.persistence.Entity;

import gestionebiblioteca.enities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Editore extends BaseEntity {
	@Column(unique = true)
	private String nomeEditore;
}
