package gestionebiblioteca.enities.data;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gestionebiblioteca.enities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Libro extends BaseEntity {
	
	private String titolo;
	
	@JsonIgnore
	@ToString.Exclude
	@ManyToOne
	private Editore editore;
	
	private LocalDate dataPubblicazione;
	@Column(unique = true)
	private Long codiceISBN;
	@Column(unique = true)
	private Integer codiceInterno;
	private Integer copieDiponibili;
	
	@JsonIgnore
	@ToString.Exclude
	@ManyToOne
	private Lingua lingua;
	
	@JsonIgnore
	@ToString.Exclude
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(
	        name="libro_categoria",
	        joinColumns= @JoinColumn(name="libro_id", referencedColumnName="id"),
	        inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
	    )
	private Set<Categoria> categoria;
	
	@JsonIgnore
    @ToString.Exclude
    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(
	        name="libro_autore",
	        joinColumns= @JoinColumn(name="libro_id", referencedColumnName="id"),
	        inverseJoinColumns= @JoinColumn(name="autore_id", referencedColumnName="id")
	    )
    private Set<Autore> autore;
	
	private Integer numeroPagine;
	
}