package gestionebiblioteca.enities.model;

public enum Periodicita {
	QUOTIDIANO, MENSILE, ANNUALE, ESTEMPORANEA
}
