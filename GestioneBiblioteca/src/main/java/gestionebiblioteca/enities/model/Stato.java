package gestionebiblioteca.enities.model;

public enum Stato {
	IN_CONSULTAZIONE, IN_PRESTITO, DISPONIBILE
}
