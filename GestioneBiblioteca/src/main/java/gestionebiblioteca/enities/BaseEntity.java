package gestionebiblioteca.enities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@Data
@MappedSuperclass
public class BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(
			columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", 
			insertable = false, 
			updatable = false)
	private Date createdAt;

}
